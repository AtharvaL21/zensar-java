import java.util.Scanner;

class Account{
	private int balance;
	
	public Account(){
		balance=0;
	}
	
	public void deposite(int amnt)
	{
		balance=balance+amnt;
		System.out.println("Amount Deposited Successfully");
	}
	
	public void withdraw(int amnt) throws Exception
	{
		if(amnt>15000)
			throw new Exception("Overlimit....");
		else if(amnt>balance)
			throw new Exception("Insufficient Balance....");
		
		balance=balance-amnt;
		System.out.println("Successfully withdrew Rs."+amnt);
	}
}

public class Assgn_30 {

	public static void main(String[] args) {
		int dep,amnt;
		Account acc = new Account();
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter the Amount to deposit:");
		dep=sc.nextInt();
		acc.deposite(dep);
		System.out.println("Enter the amount to withdraw:");
		amnt=sc.nextInt();
		try 
		{
			acc.withdraw(amnt);
		}
		catch(Exception e)
		{
			System.out.println(e);
		}
					
	}

}
