
public class Assgn_19 {

	public static void main(String[] args) {
		Student s1 = new Student(1,"xyz",90);
		Student s2 = new Student(2,"abc",88);
		Student s3 = new Student(3,"qwe",92);
		Student s4 = new Student(4,"sdf",85);
		
		Student.showCnt();  //Calling Static block without object
	}

}

class Student{
	private int roll_no;
	private String name;
	private float percentage;
	private static int cnt;
	
	static
	{
		cnt=0;
	} 
	public Student(int roll_no, String name, float percentage) //Constructor with same name but different paramaters 
	{
		this.roll_no=roll_no;
		this.name=name;
		this.percentage=percentage;
		cnt++;
	}
	
	public static void showCnt()  //calling show count without using object
	{
		System.out.println("Number of objects :- "+cnt);
	}

}
