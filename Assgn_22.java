
public class Assgn_22 {

	public static void main(String[] args) {
		Student2 s1 = new Student2("Atharva",21,01,2000);
		Student2 s2 = new Student2("abc",12,11,1998);
		s1.show();
		s2.show();
	}

}
class Student2{
	private int roll;
	private String name;
	private Date dob;
	
	
	public Student2()
	{
		roll=0;
		name=null;
		
	}
	public Student2(String n,int d,int m,int y){
		roll++;
		name=n;
		dob = new Date(d,m,y);
	}
	

	public void show()
	{
		System.out.println(roll);
		System.out.println(name);
		dob.display();
		System.out.println("\n");
		
	}
	
}
class Date{

	private int dd,mm,yy;
	
	public Date(int d, int m, int y)
	{
		dd=d;
		mm=m;
		yy=y;
	}
	public void display()
	{
		System.out.println(dd+"/"+mm+"/"+yy);
	}

}