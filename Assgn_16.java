import java.util.Scanner;

public class Assgn_16 {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		int [][]a;
		int [][]b;
		int [][]c = new int[3][3];
		System.out.println("Enter the 1st matrix");
		System.out.println("Enter the number of rows");
		a=new int[sc.nextInt()][];
		System.out.println("Enter the number of columns");
		for(int i=0;i<a.length;i++)
		{
			a[i]=new int[sc.nextInt()];
		}
		System.out.println("Enter the elements");
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				a[i][j]=sc.nextInt();
			}
		}
		System.out.println("First Matrix is:");
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<a[i].length;j++)
			{
				System.out.print(a[i][j]+"   ");
			}
			System.out.println();
		}
		System.out.println("Enter the 2nd matrix");
		System.out.println("Enter the number of rows");
		b=new int[sc.nextInt()][];
		System.out.println("Enter the number of columns");
		for(int i=0;i<b.length;i++)
		{
			b[i]=new int[sc.nextInt()];
		}
		System.out.println("Enter the elements");
		for(int i=0;i<b.length;i++)
		{
			for(int j=0;j<b[i].length;j++)
			{
				b[i][j]=sc.nextInt();
			}
		}
		System.out.println("Second matrix is:");
		for(int i=0;i<b.length;i++)
		{
			for(int j=0;j<b[i].length;j++)
			{
				System.out.print(b[i][j]+"   ");
			}
			System.out.println();
		}
		
		//-----------------MULTIPLICATION LOGIC-------------------------
		for(int i=0;i<a.length;i++)
		{
			for(int j=0;j<b.length;j++)
			{
				for(int k=0;k<b.length;k++)
				{
					c[i][j]+=a[i][k]*b[k][j];
				}
			}
		}
		System.out.println("Multiplication of both matrices:");
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				System.out.print(c[i][j]+"   ");
			}
			System.out.println();
		}
	}

}
