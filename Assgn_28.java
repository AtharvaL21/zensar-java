import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Assgn_28 {

	public static void main(String[] args) {
		List<employee_28> e = new ArrayList<>();
		Scanner sc =new Scanner(System.in);
		int i,ch1,id,num=0;
		String nm,ch2;
		float sal;
		do
		{
			System.out.println("Enter your choice:\n1.Insert Record\n2.Update Data\n3.Display all records");
			ch1=sc.nextInt();
			switch(ch1){
			case 1:
				System.out.println("\nEnter Employee id:");
				id=sc.nextInt();
				System.out.println("Enter the name of employee:");
				nm=sc.next();
				System.out.println("Enter employee salary:");
				sal=sc.nextFloat();
				
				e.add(new employee_28(id,nm,sal));
				System.out.println("Employee Added");
				++num;
				break;
			
			case 2:
				System.out.println("Enter the Employee Id:");
				id=sc.nextInt();
				
				for(employee_28 x:e)
				{
					if(id==x.getId())
					{
						//System.out.println("What do you want to Update: id,name,salary");
						//ch2=sc.next();
						//if(ch2=="id")
						{
							System.out.println("Enter the updated id");
							id=sc.nextInt();
							x.setEmployee_id(id);
						}
						//else if(ch2=="name")
						{
							System.out.println("Enter the updated Name");
							nm=sc.next();
							x.setName(nm);
						}
						//else if(ch2=="salary")
						{
							System.out.println("Enter the updated Salary");
							sal=sc.nextFloat();
							x.setSal(sal);
						}
						
					}
				}
				break;
			case 3:
				System.out.println("\nDetails:");
				for(employee_28 y:e)
				{
					y.display();
				}
				break;
			case 4:
				System.out.println("Exiting..........");
				break;
			}
		}while(ch1!=4);
	}

}
//employee id, employee name and salary

class employee_28<E>{
	private int employee_id;
	private String name;
	private float sal;
	
	public employee_28()
	{
		employee_id=0;
		name=null;
		sal=0;
	}
	public int getId() {
		return employee_id;
	}
	public employee_28(int employee_id, String name, float sal) {
		super();
		this.employee_id = employee_id;
		this.name = name;
		this.sal = sal;
	}
	
	public void setEmployee_id(int employee_id) {
		this.employee_id = employee_id;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setSal(float sal) {
		this.sal = sal;
	}
	public void display()
	{
		System.out.println("Employee id: "+employee_id);
		System.out.println("Employee Name: "+name);
		System.out.println("Employee Salary: "+sal);
	}
	
}