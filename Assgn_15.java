import java.util.Scanner;

public class Assgn_15 {

	public static void main(String[] args) {
		Scanner sc =new Scanner(System.in);
		int [][] c;
		System.out.println("Enter the number of rows\n");
		c=new int[sc.nextInt()][];
		System.out.println("Enter the number of columns\n");
		for(int i=0;i<c.length;i++)
		{
			c[i]=new int[sc.nextInt()];
		}
		System.out.println();
		System.out.println("Enter elements");
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				c[i][j]=sc.nextInt();
			}
		}
		System.out.println("Matrix is:");
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				System.out.print(c[i][j]+" ");
			}
			System.out.println();
		}
		//----------------TRANSPOSE----------------------
		System.out.println("Transpose of Matrix: ");
		
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				System.out.print(c[j][i]+" ");
			}
			System.out.println();
		}
		//----------------ADDITITON---------------------
		
		System.out.println("Enter other matrix for addition");
		int [][] d ;
		System.out.println("Enter the number of rows\n");
		d=new int[sc.nextInt()][];
		System.out.println("Enter the number of columns\n");
		for(int i=0;i<d.length;i++)
		{
			d[i]=new int[sc.nextInt()];
		}
		System.out.println();
		System.out.println("Enter elements");
		for(int i=0;i<d.length;i++)
		{
			for(int j=0;j<d[i].length;j++)
			{
				d[i][j]=sc.nextInt();
			}
		}
		System.out.println("Array is:");
		for(int i=0;i<d.length;i++)
		{
			for(int j=0;j<d[i].length;j++)
			{
				System.out.print(d[i][j]+" ");
			}
			System.out.println();
		}
			//---------ADDITION LOGIC-------------
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				d[i][j]+=c[i][j];
			}
		}
		
		System.out.println("Addition of both matrices is:");
		for(int i=0;i<c.length;i++)
		{
			for(int j=0;j<c[i].length;j++)
			{
				System.out.print(d[i][j]+"  ");
			}
			System.out.println();
		}

	}

}
