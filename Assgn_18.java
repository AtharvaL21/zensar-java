import java.util.Scanner;

public class Assgn_18 {

	public static void main(String[] args) {
		int i=0,flag=0,ch;
		AccountHolder [] m = new AccountHolder[10];
		Scanner sc =new Scanner(System.in);
		do 
		{
			System.out.println("Enter your choice:\n1.Add record\n2.Display record\n3.Deposite Amount\n4.Withdraw Amount\n5.Exit");
			ch=sc.nextInt();
			switch(ch)
			{
			case 1:
				m[i]=new AccountHolder();
				System.out.println("Add records of account holder\n");
				System.out.println("Account Holder Number:");
				m[i].setacc_num(sc.nextInt());
				System.out.println("Account Holder Name");
				m[i].setName(sc.next());
				i++;
				System.out.println("\n*******Record added successfully*******");
				break;
				
			case 2:
				System.out.println("Customer Details:\n");
				for(int j=0;j<i;j++)
				{
					System.out.println("---------Customer No:"+(j+1)+"----------1");
					m[j].show();
				}
				break;
				
			case 3:
				System.out.println("Enter the Account Number to deposite");
				int acc=sc.nextInt();
				for(int j=0;j<i;j++)
				{
					if(acc==m[j].getacc_num())
					{
						System.out.println("Enter the amount to deposite\n");
						m[j].setBal(sc.nextInt());
						flag=1;
					}	
				}
				if(flag==0)
					System.out.println("Invalid Account number");
				
				break;
			case 4:
				System.out.println("Enter the Account Number to withdraw");
				int acc2=sc.nextInt();
				for(int j=0;j<i;j++)
				{
					if(acc2==m[j].getacc_num())
					{
						m[j].withdraw();
						flag=1;
					}	
				}
				if(flag==0)
					System.out.println("Invalid Account number");
				
				break;
				
			}
			
		}while(ch!=5);


	}

}

class AccountHolder{
	private int acc_num;
	private String name;
	private int balance;
	Scanner sc1 = new Scanner(System.in);
	public AccountHolder()
	{
		acc_num=0;
		name=null;
		balance=0;
	}
	
	
	public void setName(String nm)
	{
		name=nm;
	}
	public String getName()
	{
		return name;
	}
	
	public void setacc_num(int num)
	{
		acc_num=num;
	}
	public int getacc_num()
	{
		return acc_num;
	}
	
	public void setBal(int amnt)
	{
		balance+=amnt;
	}
	public int getBal()
	{
		return balance;
	}
	public void withdraw()
	{
		System.out.println("Enter the amount to withdraw\n");
		int wdraw=sc1.nextInt();
		if(wdraw>balance)
		{
			System.out.println("Insufficient Amount\n");
		}
		else
		{
			balance-=wdraw;
			System.out.println("Rs."+wdraw+" Withdrawn Successfully\n");
		}
	}
	
	public void show()
	{
		System.out.println("Customer Name: "+name+"\nAccount Number: "+acc_num+"\nBalance: "+balance+"\n");
		
	}
	
	
}
