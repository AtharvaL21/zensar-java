import java.awt.*;

class moveCircles_3 extends Frame implements Runnable
{
	private Thread t1,t2,t3;
	private int x1,x2,x3;
	
	public moveCircles_3()
	{
		x1=x2=x3=100;
		Thread t1= new Thread(this,"red");
		Thread t2= new Thread(this,"blue");
		Thread t3= new Thread(this,"green");
		t1.start();
		t2.start();
		t3.start();
	}
	
	public void paint(Graphics g)
	{
		//
		g.setColor(Color.RED);
		g.fillOval(x1, 100, 50, 50);
		//
		g.setColor(Color.BLUE);
		g.fillOval(x2, 200, 50, 50);
		//
		g.setColor(Color.GREEN);
		g.fillOval(x3, 300, 50, 50);
	}
	
	public void run() {
		while(true)
		{
	//-------------------------Circle - RED----------------------------------
			if(Thread.currentThread()==t1)
			{
				x1++;
				if(x1==this.getWidth()-75)
				{
					//---------FOR WAITING--------------
					synchronized (this)
					{
						try 
						{
							wait();
						} 
						catch (Exception e) 
						{
							e.printStackTrace();
						}
					}	
				}
				try 
				{
					Thread.sleep(20);
				} 
				catch (Exception e)
				{
					e.printStackTrace();
				}
					//------------------------------------
			}
				
				
	//-------------------Circle - BLUE---------------------------------------			
			else if(Thread.currentThread()==t2)
			{
				x2++;
				if(x2==this.getWidth()-75)
				{
					//----------------FOR WAITING------------------
					synchronized (this)
					{
						try 
						{
							wait();
						}
						catch (Exception e) 
						{
							e.printStackTrace();
						}
					}
					//-----------------------------------------------
				}
				try 
				{
					Thread.sleep(50);
				} catch (Exception e) 
				{
					e.printStackTrace();
				}
			}
					
				
	//----------------------Circle - Green------------------------------------			
			else if(Thread.currentThread()==t3)
			{
				x3++;
				if(x3==this.getWidth()-75)
				{
					synchronized (this)
					{
						x1=x2=x3=100;
						notifyAll();
					}
				}
				try 
				{
					Thread.sleep(70);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
			}
				
			repaint();	
		}
		
	}
	
}

public class Assgn_33 {

	public static void main(String[] args) {
		moveCircles_3 mc = new moveCircles_3();
		mc.setSize(600,600);
		mc.setVisible(true);
		
	}

}
