import java.util.Scanner;

public class Assgn_14 {

	public static void main(String[] args) {
		int j=0,k;
		Scanner sc = new Scanner(System.in);
		int[] b = new int[5]; //Uninitialized array
		System.out.println("Enter array of 5 elements");
		for(int i=0;i<b.length;i++)
		{
			b[i]=sc.nextInt();
		}
		//-----------------Max and Min---------
		k=b[0];
		System.out.println("Array elements :-");
		for(int i=0;i<b.length;i++) //Printing using FOR loop
		{
			System.out.println(b[i]+" ");
		}
		System.out.println();
		for(int i=0;i<b.length;i++)
		{
			if(b[i]>j)
			{
				j=b[i];
			}
		}
		System.out.println("Largest element is:" +j);
		System.out.println();
		for(int i=0;i<b.length;i++)
		{
			if(k>b[i])
			{
				k=b[i];
			}
		}
		System.out.println("Minimum element is:" +k);
		
		//-------------Multiply by 5--------------
		int [] c = new int[b.length];
		for(int i=0;i<b.length;i++)
		{
			c[i]=b[i]*5;
		}
		System.out.println("New Array:");
		for(int i=0;i<c.length;i++)
		{
			System.out.println(c[i]+" ");
		}

	}

}
