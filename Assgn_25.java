
public class Assgn_25 {

	public static void main(String[] args) {
		printable c = new CktPlayer("Modi",75);
		c.printDetails();
		
		printable f = new FtPlayer("Amit",3);
		f.printDetails();
		
	}

}

interface printable{
	public abstract void printDetails();
}

class CktPlayer implements printable{
	private String name;
	private int runs;
	
	public CktPlayer(String nm,int r)
	{
		name=nm;
		runs=r;
	}
	
	public void printDetails()
	{
		System.out.println("\nPlayer Name: "+name);
		System.out.println("Runs Scored: "+runs);
	}
}

class FtPlayer implements printable{
	private String name;
	private int goals;
	
	
	public FtPlayer(String name, int goals) {
		this.name = name;
		this.goals = goals;
	}


	public void printDetails()
	{
		System.out.println("\nPlayer Name: "+name);
		System.out.println("Goals Scored: "+goals);
	}
}
