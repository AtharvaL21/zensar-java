import java.util.*;

public class Assgn_29 {

	public static void main(String[] args) {
		UtilityReport u = new UtilityReport();
		//UtilityReport ur2 = new UtilityReport();
		u.createList();
		System.out.println();
		u.printList();
		System.out.println();
		u.showReport();
	}

}

class Student_29
{
	private int rollno;
	private String name;
	private double percentage;
	
	public Student_29(int rollno, String name, double percentage) {
		this.rollno = rollno;
		this.name = name;
		this.percentage = percentage;
	}
	
	@Override
	public String toString() {
		return "rollno = " + rollno + " name = " + name + " percentage = " + percentage;
	}

	public Student_29()
	{
		rollno=0;
		name=null;
		percentage=0;
	}

	public String getName() {
		return name;
	}

	public double getPercentage() {
		return percentage;
	}

	
}

class UtilityList
{
	int num;
	private List<Student_29> list = new LinkedList<>();
	Scanner sc=new Scanner(System.in);
	public void createList()
	{
			list.add(new Student_29(1,"Atharva ",89));
			list.add(new Student_29(2,"ABC ",90));
			list.add(new Student_29(3,"QWE ",95));
	}
	
	public void printList()
	{
		for(Student_29 x:list)
			System.out.println(x);
			
		System.out.println();
	}

	
	public List<Student_29> getList() {
		return list;
	}


}

class UtilityReport extends UtilityList{
	Map<String, Double> ur = new TreeMap<String, Double>();
	List<Student_29> l2 =getList(); 
	public void showReport()
	{
		for(Student_29 y:l2)
		{
			ur.put(y.getName(), y.getPercentage());
		}
		for(Map.Entry<String, Double> entry : ur.entrySet()) 
		{
			System.out.println(entry.getKey() + " = "+ entry.getValue());
		}
	}
	
	
}
