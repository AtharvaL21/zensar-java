package com.cdac.pack2;

public class Batch {
	private String CourseName;
	private int BatchStrength;
	
	public Batch()
	{
		CourseName=null;
		BatchStrength=0;
	}
	
	public Batch(String c,int b) {
		CourseName=c;
		BatchStrength=b;
	}
	
	public void show()
	{
		System.out.println("Course Name: "+CourseName+"  Batch Strength: "+BatchStrength);
	}

}
