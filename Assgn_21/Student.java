package com.cdac.pack1;

public class Student {
	private int rollno;
	private String name;
	
	public Student()
	{
		rollno=0;
		name=null;
	}
	
	public Student(int r, String n)
	{
		rollno=r;
		name=n;
	}
	
	public void display()
	{
		System.out.println("Name: "+name+"  Roll no: "+rollno);
	}
}
