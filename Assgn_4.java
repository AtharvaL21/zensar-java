
public class Assgn_4 {

	public static void main(String[] args) {
	//Using 3rd Variable
		int temp,a=10,b=20;
		System.out.println("Using Variable\nNums before swaping:\na:"+a+"  b:"+b);
		temp=a;
		a=b;
		b=temp;
		System.out.println("Nums after swaping:\na:"+a+"  b:"+b);
		
	//Without using 3rd variable
		int n1=20,n2=30;
		System.out.println("\nWithout 3rd variable\nNums before swaping:\nNum1:"+n1+"  Num2:"+n2);
		n1=n1+n2;
		n2=n1-n2;
		n1=n1-n2;
		System.out.println("Nums after swaping:\nNum1:"+n1+"  Num2:"+n2);

		
	}

}
