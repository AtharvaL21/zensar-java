class incremented extends Thread
{
	public incremented(int num)
	{
		System.out.println("10 Incrementations of number:"+num);
		for(int i=1;i<=10;i++)
		{
			System.out.println(num++);
		}
	}
}

class multiplied extends Thread
{
	public multiplied(int num)
	{
		System.out.println("Multiplication of num\n");
		for(int i=1;i<10;i++)
		{
			System.out.println(num+"*"+i+"="+(num*i));
		}
	}
}

public class Assgn_31 {

	public static void main(String[] args) {
		incremented inc = new incremented(10);
		multiplied mul = new multiplied(5);
		
		inc.start();
		mul.start();

	}

}
