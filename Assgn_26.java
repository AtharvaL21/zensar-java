
public class Assgn_26 {

	public static void main(String[] args) throws CloneNotSupportedException {
		vehicle v1 = new vehicle(1,"Bently",999999);
		vehicle v2 = new vehicle(1,"Bently",999999);
		vehicle v3=(vehicle)v1.clone();
		System.out.println(v3.toString());
		if(v1.equals(v2))
		{
			System.out.println("Same");
		}
		else 
		{
			System.out.println("Not Same");
		}
	}

}

class vehicle implements Cloneable
{
	 private int number; 
	 private String name;
	 private float price;
	 
	 public vehicle(){
		 number=0;
		 name=null;
		 price=0;
	 }

	public vehicle(int number, String name, float price) {
		super();
		this.number = number;
		this.name = name;
		this.price = price;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + number;
		result = prime * result + Float.floatToIntBits(price);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		vehicle other = (vehicle) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (number != other.number)
			return false;
		if (Float.floatToIntBits(price) != Float.floatToIntBits(other.price))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "vehicle [number=" + number + ", name=" + name + ", price=" + price + "]";
	}
	 
	public Object clone() throws CloneNotSupportedException
	{
		return super.clone();
	}
}