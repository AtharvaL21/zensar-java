
public class Assgn_23 {

	public static void main(String[] args) {
		WageEmployee e1 = new WageEmployee(1,"Atharva",21,01,2000,20,500); //id,name,dob,hours,rate
		e1.display();
		SalesPerson s1 = new SalesPerson(2,"Langhe",16,04,2004,10,300,20,50); //id,name,dob,hours,rate,item,comm
		s1.show();

	}

}

class employee{
	private int empid;
	private String name;
	private Date2 dob;
	
	public employee()    //new employee
	{
		empid=1;
		name="abc";
		dob=new Date2();
	}
	
	public employee(int id,String n,int d,int m,int y)
	{
		empid=id;
		name=n;
		dob=new Date2(d,m,y);
	}
	
	public void display()
	{
		System.out.println("\n"+empid);
		System.out.println(name);
		dob.display();
	}
}
//------------------------WAGE EMPLOYEE-------------------------
class WageEmployee extends employee {
	private int hours;
	private int rate;
	private int wage_sal;
	
	public WageEmployee() 
	{
		hours=0;
		rate=0;
		System.out.println("Wage Employee Constructor");
	}
	
	public WageEmployee(int id,String n, int d,int m,int y,int h,int r)
	{
		super(id,n,d,m,y);     //To call employee class//should be first before anything
		hours=h;
		rate=r;
		wage_sal = hours*rate;
	}
	public void display()
	{
		super.display();
		System.out.println("Hours worked"+hours);
		System.out.println("Rate per hour"+rate);
		System.out.println("Salary: "+wage_sal);
	}
}
//-------------------------------SALESPERSON-------------------------------------
class SalesPerson extends WageEmployee{
	private int itemsold;
	private int comm_item;
	private int sales_sal;
	
	public SalesPerson()
	{
		itemsold=0;
		comm_item=0;
		System.out.println("SalesPerson class constructor");
	}
	
	public SalesPerson(int id,String n, int d,int m,int y,int h,int r,int i,int c)
	{
		super(id,n,d,m,y,h,r);
		itemsold=i;
		comm_item=c;
		sales_sal = (h*r)+(itemsold*comm_item);
	}
	
	public void show()
	{
		super.display();
		System.out.println("---SalesDetails---\nNumber of Items Sold"+itemsold);
		System.out.println("Commission per item"+comm_item);
		System.out.println("Salary:"+sales_sal);
	}
}

//------------------------------DATE----------------------------------------
class Date2{

	private int dd,mm,yy;
	public Date2()
	{
		dd=1;
		mm=1;
		yy=2000;
	}
	public Date2(int d, int m, int y)
	{
		dd=d;
		mm=m;
		yy=y;
	}
	public void display()
	{
		System.out.println(dd+"/"+mm+"/"+yy);
	}

}


