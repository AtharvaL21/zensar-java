
public class Assgn_20 {

	public static void main(String[] args) {
		Student_20 s1 = new Student_20(1,"xyz",90);
		Student_20 s2 = new Student_20(2,"abc",88);
		Student_20 s3 = new Student_20(3,"qwe",92);
		Student_20 s4 = new Student_20(4,"sdf",85);
		s1.setName("Gandhi");
		System.out.println(s1.getName());
		s2.setName("Modi");
		System.out.println(s2.getName());
		s3.setName("Pawar");
		System.out.println(s3.getName());
		s4.setName("Shah");
		System.out.println(s4.getName());
		Student_20.showCnt();  //Calling Static block without object
	}

}

class Student_20{
	private int roll_no;
	private String name;
	private float percentage;
	private static int cnt;
	
	static
	{
		cnt=0;
	} 
	public Student_20(int roll_no, String name, float percentage) //Constructor with same name but different paramaters 
	{
		this.roll_no=roll_no;
		this.name=name;
		this.percentage=percentage;
		cnt++;
	}
	
	public static void showCnt()  //calling show count without using object
	{
		System.out.println("Number of objects :- "+cnt);
	}

	public int getRoll_no() {
		return roll_no;
	}

	public void setRoll_no(int roll_no) {
		this.roll_no = roll_no;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPercentage() {
		return percentage;
	}

	public void setPercentage(float percentage) {
		this.percentage = percentage;
	}
	
	
}
