
public class Assgn_24 
{
	public static void main(String[] args) 
	{
		employee_24 e = new employee_24(1,"Trump",2500);
		Manager m = new Manager(2,"Obama",3000);
		MarketingExecutive me = new MarketingExecutive(3,"Modi",2800,500);
		printObject(e,m,me);
	}


	static void printObject(employee_24 e,Manager m,MarketingExecutive me)
	{
		e.display_emp();
		m.display_manager();
		me.display_market();		
	}
}

class employee_24
{
	private int empid;
	private String name;
	private double basic_sal;
	
	public employee_24()
	{
		empid=0;
		name=null;
		basic_sal=0;
	}

	public employee_24(int empid, String name, double basic_sal) 
	{
		this.empid = empid;
		this.name = name;
		this.basic_sal = basic_sal;
	}
	
	public void display_emp()
	{
		System.out.println("\nEmployee id: "+empid);
		System.out.println("Employee Name: "+name);
		System.out.println("Basic Salary: "+basic_sal);
	}
	public double getBasic_sal() {
		return basic_sal;
	}
	
	public int getempid()
	{
		return empid;
	}
	public String getName()
	{
		return name;
	}
	public double getbasic_sal()
	{
		return basic_sal;
	}
	
	
}

class Manager extends employee_24{
	private double petrol_allowance;// 8% of basic salary 
	private double food_allowance; //12% of basic salary
	private double other_allowance; //4% of basic salary
	private double GrossSalary;
	private double NetSalary;
	private double PF;
	public Manager() 
	{
		petrol_allowance = 0;
		food_allowance=0;
		other_allowance=0;
	}
	public Manager(int eid,String nm,double bas)
	{
		super(eid,nm,bas);
		petrol_allowance=0.08*bas;
		food_allowance=0.12*bas;
		other_allowance=0.04*bas;
		PF=0.125*bas;
		
	}
	
	public double calculateGrossSalary()
	{
		return GrossSalary = super.getBasic_sal()+(petrol_allowance+food_allowance+other_allowance);
				//net salary = gross salary - PF
				//PF = 12.5% of basic salary
	}
	
	public double calculateNetSalary()
	{
		return NetSalary=GrossSalary - PF;
	}
	
	public void display_manager()
	{
		super.display_emp();
		System.out.println("Manager Gross Salary: "+calculateGrossSalary());
		System.out.println("Manager Net Salary: "+calculateNetSalary());
	}
}

class MarketingExecutive extends employee_24
{
	private float kilometers;
	private float tour_allowance; //Rs5 per KM
	private int telephone_allowance;
	private double GrossSalary;
	private double NetSalary;
	private double PF;
	
	public MarketingExecutive() {
		kilometers=0;
		tour_allowance=0;
		telephone_allowance=2000;
	}
	
	public MarketingExecutive(int eid,String nm,double bas,float kilo)
	{
		super(eid,nm,bas);
		kilometers=kilo;
		tour_allowance=5*kilometers;
		PF=0.125*bas;
	}
	
	public double calculateGrossSalary()
	{
		return GrossSalary = super.getBasic_sal()+(tour_allowance+telephone_allowance);
				//net salary = gross salary - PF
				//PF = 12.5% of basic salary
	}
	public double calculateNetSalary()
	{
		return NetSalary=GrossSalary - PF;
	}
	
	public void display_market()
	{
		super.display_emp();
		System.out.println("Marketing Executive Gross Salary: "+calculateGrossSalary());
		System.out.println("Marketing Executive Net Salary: "+calculateNetSalary());
	}
}
	
